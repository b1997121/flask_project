import json
import jwt
from datetime import datetime, timedelta
from flask import Blueprint, request, render_template

ama_api = Blueprint('ama_api', __name__, template_folder='templates')


@ama_api.route('/key_cycling')
def index():
    now = datetime.today()
    return render_template(
        'index.html',
        index=True,
        cur_time=now.strftime("%c"),
        two_hr=(now + timedelta(hours=2)).strftime("%c"),
        value=''
    )


@ama_api.route('/key_cycling', methods=['POST'])
def gen_key_cycling():
    value = ''
    form_dict = request.form
    print(form_dict)
    payload = {
        'chassis_SN': form_dict.get("chassis_sn", None),
        'FRU_comparison_VRCID': form_dict.get("fru_id", None),
        'sys_log_comparison_VRCID': form_dict.get("sys_log_id", None),
        'SDR_comparison_VRCID': form_dict.get("sdr_id", None),
        'expire': form_dict.get("expire_date", None),
        'create': form_dict.get("create_date", None),
        'SKU_HW_CFG_check': form_dict.get("cfg_chk", None),
    }


    if None not in payload.values():
        payload['create_timestamp'] = datetime.strptime(payload['create'], "%c").strftime("%s")
        payload['expire_timestamp'] = datetime.strptime(payload['expire'], "%c").strftime("%s")
        value = jwt.encode(payload=payload, key='AMA_encrypt_key_by_robot', algorithm='HS256')

    print(json.dumps(payload, indent=4))
    print(value)

    now = datetime.today()

    return render_template(
        'index.html',
        index=True,
        cur_time=now.strftime("%c"),
        two_hr=(now + timedelta(hours=2)).strftime("%c"),
        value=value
    )
